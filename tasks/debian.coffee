gulp = require 'gulp'
deb = require 'gulp-deb'
{ resolve } = require 'path'
pkg = require resolve process.cwd(), 'package.json'

gulp.task 'debian', ->
   gulp.src([
     'public_html/**'
   ], { base: process.cwd() })
     .pipe(deb("#{pkg.name}.deb", {
       name: pkg.name,
       version: pkg.version,
       maintainer: {
         name: pkg.author or '',
         email: pkg.email or ''
       },
       short_description: pkg.description or ''
     }))
     .pipe(gulp.dest('.'))
