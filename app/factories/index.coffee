angular = require 'angular'

app = angular.module "app.factories", []

app.factory "MasterPage", require './master-page'

app.factory "Socket", (config, socketFactory) ->

  socketFactory ioSocket: io.connect config.io.url
