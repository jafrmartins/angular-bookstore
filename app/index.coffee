window.$ = window.jQuery = require 'jquery'
window._ = window.lodash = require 'lodash'
window.io = window.socketIO = require 'socket.io-client'

require 'angular'
require 'bootstrap'
require 'angular-socket-io'
require 'angular-animate'
require 'angular-resource'
require 'angular-cookies'
require 'angular-sanitize'
require 'angular-touch'
require 'angular-ui-router'
require 'angular-ui-router-tabs'
require 'angular-ui-bootstrap/ui-bootstrap'
require 'angular-ui-bootstrap/ui-bootstrap-tpls'
require 'angular-translate'
require 'angular-translate-loader-partial'

require './constants'
require './services'
require './factories'
require './controllers'
require './directives'
require './pages'

app = angular.module 'app', [
  'ngSanitize',
  'ngResource',
  'ngCookies',
  'ngAnimate',
  'ngTouch',
  'ui.bootstrap',
  'ui.bootstrap.tpls',
  'ui.router',
  'ui.router.tabs',
  'pascalprecht.translate',
  'btford.socket-io',
  'app.constants',
  'app.services',
  'app.factories',
  'app.controllers',
  'app.directives',
  'app.pages'
]

app.config (router, $stateProvider, $urlRouterProvider) ->
  Object.keys(router).map (state) ->
    unless state is 'otherwise' then $stateProvider.state(
      state, router[state]
    )
  $urlRouterProvider.otherwise router.otherwise.url

app.config ($translateProvider, languagePickerOptions) ->
  $translateProvider.useLoader '$translatePartialLoader',
    urlTemplate: 'i18n/{part}-i18n-{lang}.json'

  $translateProvider.registerAvailableLanguageKeys(
    languagePickerOptions.langs, languagePickerOptions.locales
  )

  $translateProvider.determinePreferredLanguage()
  $translateProvider.useSanitizeValueStrategy('sanitize')

app.run (
  $state,
  $rootScope,
  $stateParams,
  $translate,
  config,
  MasterPage,
  Socket
) ->

  $rootScope.$state = $state
  $rootScope.$stateParams = $stateParams
  $rootScope.MasterPage = MasterPage
  $rootScope.Socket = Socket

  config.io.forward.map (event) ->
    $rootScope.Socket.forward event

  $rootScope.$on "$translatePartialLoaderStructureChanged", ->
    $translate.refresh()
