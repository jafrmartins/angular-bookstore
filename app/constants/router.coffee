module.exports =

  'otherwise':

    url: '/bookstore/books'

  'site':

    abstract: true

    views:

      '@':

        templateUrl: 'pages/master/index.html'

      'header@site':

        templateUrl: 'pages/master/header.html'

      'footer@site':

        templateUrl: 'pages/master/footer.html'

  'site.bookstore':

    abstract: true

    url: '/bookstore'

    templateUrl: 'pages/bookstore/index.html'

  'site.bookstore.view':

    url: '/books/:id/view'

    controller: 'BookViewController'

    templateUrl: 'pages/bookstore/book-view.html'

  'site.bookstore.edit':

    url: '/books/:id/edit'

    controller: 'BookEditController'

    templateUrl: 'pages/bookstore/book-edit.html'

  'site.bookstore.add':

    url: '/books/add'

    controller: 'BookAddController'

    templateUrl: 'pages/bookstore/book-add.html'

  'site.bookstore.list':

    url: '/books'

    controller: 'BookListController'

    templateUrl: 'pages/bookstore/book-list.html'
