angular = require 'angular'

app = angular.module 'app.constants', []

app.constant 'config', require './config'

app.constant 'router', require './router'

app.constant 'navbar', require './navbar'
