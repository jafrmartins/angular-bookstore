module.exports =

  'api':
    url: "http://localhost:3000/api/v1"

  'io':
    url: "http://localhost:3000"
    forward: [
      "put:books",
      "del:books",
      "post:books"
    ]