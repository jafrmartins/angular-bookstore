module.exports = class BookViewController

  constructor: ($scope, $stateParams, Book) ->

      $scope.book = Book.get id: $stateParams.id
