module.exports = class BookCreateController

  constructor: ($scope, $state, $stateParams, Book) ->

    $scope.book = new Book()

    $scope.addBook = ->
      $scope.book.$save ->
        $state.go('site.bookstore.list')
