module.exports = class BookListController

  constructor: ($scope, $state, $window, Book, config) ->

      $scope.books = Book.query()
      config.io.forward.map (event) ->
        $scope.$on "socket:#{event}", ->
          $scope.books = Book.query()

      $scope.deleteBook = (book) ->
          if confirm("delete #{book.title}?")
              book.$delete ->
                $scope.books = Book.query()
