module.exports = class BookEditController

  constructor: ($scope, $state, $stateParams, Book) ->

    $scope.updateBook = ->
      $scope.book.$update ->
        $state.go('site.bookstore.list')

    $scope.loadBook = ->
        $scope.book = Book.get({id:$stateParams.id});

    $scope.loadBook()
