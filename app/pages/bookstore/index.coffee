angular = require 'angular'

app = angular.module 'app.pages.bookstore', ["ngResource"]

app.factory 'Book',($resource, config) ->
    $resource "#{config.api.url}/books/:id",{id:'@_id'},{
        update: {
            method: 'PUT'
        }
    }

app.controller "BookListController", require './controllers/book-list'

app.controller "BookAddController", require './controllers/book-add'

app.controller "BookEditController", require './controllers/book-edit'

app.controller "BookViewController", require './controllers/book-view'

app.controller "BookstoreController",  class BookstoreController

    constructor: (
      Book,
      @$scope,
      $translatePartialLoader
    ) ->

      $translatePartialLoader.addPart 'bookstore'
      @$scope.MasterPage.setTitle 'BOOKSTORE'
