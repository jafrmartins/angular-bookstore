angular = require 'angular'

require 'angular-navbar'
require 'angular-language-picker'

app = angular.module 'app.pages.master', [
  'app.navbar',
  'app.languagePicker'
]

app.controller "MasterController", class MasterController

  constructor: (
    $scope,
    $state,
    $stateParams,
    $translatePartialLoader
    navbar
  ) ->

    $translatePartialLoader.addPart 'master'

    $scope.MasterPage.setEncoding 'utf-8'

    $scope.MasterPage.setTitle 'BOOKSTORE'

    $scope.navbar = navbar
