angular = require 'angular'

require './master'
require './bookstore'

app = angular.module 'app.pages', [
  'app.controllers',
  'app.pages.master',
  'app.pages.bookstore'
]
