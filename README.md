# Angular Bookstore

![doodle](https://gitlab.com/jafrmartins/angular-bookstore/raw/master/doodle.png)

## Install
<pre>
git clone https://github.com/jafrmartins/angular-bookstore.git
cd angular-bookstore
npm install
</pre>

## Clean
<pre>
gulp clean
</pre>

## Bundle
<pre>
gulp bundle
</pre>

## Start
<pre>
gulp start
</pre>

## Test
<pre>
gulp test
</pre>

## Stop
<pre>
gulp stop
</pre>
