http = require 'http'
cors = require 'cors'
express = require 'express'
gulp = require 'gulp'
del = require 'del'
concat = require 'gulp-concat'
less = require 'gulp-less'
minify = require 'gulp-minify-css'
browserify = require 'gulp-browserify'
shell = require 'gulp-shell'
flatten = require 'gulp-flatten'

{ config } = require './package'

require './tasks/debian'

gulp.task 'css', ->
  gulp.src([ 'app/*.less' ])
    .pipe(less()).on('error', console.log)
    .pipe(minify())
    .pipe(concat('index.css'))
    .pipe(gulp.dest('public_html'))

gulp.task 'js', ->
  gulp.src('app/index.coffee', { read: false})
    .pipe(browserify({
        debug: true,
        transform: ['coffeeify'],
        extensions: ['.coffee']
    }))
    .pipe(concat('index.js'))
    .pipe(gulp.dest('public_html'))

gulp.task 'html', ->
  gulp.src([ 'app/**/*.html' ])
    .pipe(gulp.dest('public_html'))

gulp.task 'clean', (cb) ->
  del(['public_html'], cb)

gulp.task 'i18n', ->
  gulp.src(['./**/*-i18n-*.json', './node_modules/**/*-i18n-*.json'], { read: "false" })
    .pipe(flatten())
    .pipe(gulp.dest('public_html/i18n'))

gulp.task 'assets', ->
  gulp.src(['./node_modules/flag-sprites/dist/img/*'])
    .pipe(gulp.dest('public_html/img'))
  gulp.src(['./node_modules/bootstrap/fonts/*'])
    .pipe(gulp.dest('public_html/fonts'))

gulp.task 'http', ->
  app = express()
  app.use cors()
  app.use express.static config.http_root
  http.createServer(app).listen config.http_port

gulp.task 'watch', ->
  gulp.watch 'app/**/*.less', ['css']
  gulp.watch 'app/**/*.coffee', ['js']
  gulp.watch 'app/**/*.html', ['html']
  gulp.watch ['**/*-i18n-*.json', 'node_modules/**/*-i18n-*.json'], ['i18n']

gulp.task 'bundle', [
  'assets',
  'css',
  'html',
  'i18n',
  'js',
]

gulp.task 'start', [
  'http'
]

gulp.task 'default', [
  'http'
]
